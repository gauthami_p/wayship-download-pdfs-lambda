const serverless = require('serverless-http');
const express = require('express');
const app = express();
const Archiver = require('archiver');
const AWS = require('aws-sdk');
const Stream = require('stream').Stream;
const Moment = require('moment');
const MomentRange = require('moment-range');
const moment = MomentRange.extendMoment(Moment);
const fs = require('fs');

app.get('/', async function (req, res) {
    console.log(req);
  let toDate = req.query.toDate;
  let fromDate = req.query.fromDate;
  let typeOfLog = req.query.typeOfLog;
  let zipfile = fromDate + '-' + toDate + '.zip';
      const start = new Date(fromDate),
      end = new Date(toDate);
    const range = moment.range(moment(start), moment(end));
    let keysParams = [];
    Array.from(range.by('day')).map(date => {
      let formattedDate = typeOfLog + '/' + date.format('YYYY-MM-DD').toString() + '.pdf';
      keysParams.push(formattedDate);
    });
    AWS.config.update({
      accessKeyId: 'AKIAUSUORCDJQEUAKE6E',
      secretAccessKey: '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',
    });
    const s3 = new AWS.S3();
    const allKeys = await s3.listObjects({
      Bucket: 'wayship-pdfs',
    }).promise();
    const keys = allKeys.Contents.filter(key => keysParams.includes(key.Key));
    console.log(keys);
    const s3DownloadStreams = keys.map((key) => {
      return {
        stream: s3.getObject({
          Bucket: 'wayship-pdfs',
          Key: key.Key,
        }).createReadStream(),
        filename: key.Key,
      };
    });

    const archive = Archiver('zip');
    archive.on('error', (error) => {
      throw new Error(`${error.name} ${error.code} ${error.message} ${error.path} ${error.stack}`);
    });

    console.log('Starting upload');
    console.log(s3DownloadStreams);
    s3DownloadStreams.forEach((streamDetails) => archive.append(streamDetails.stream, {
        name: streamDetails.filename,
      }));
    archive.finalize();
    archive.pipe(res);
    res.attachment(zipfile);
    res.setHeader("Content-Type", "application/zip"); 
    res.setHeader("Content-Disposition", "attachment; filename=" + zipfile);
})

module.exports.handler = serverless(app);